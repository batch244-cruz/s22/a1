let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

function register(newUser) {
    if(registeredUsers.includes(newUser)){
            alert("Registration failed. Username already exists!")
    }else{
            registeredUsers.push(newUser);
            alert("Thank you for registering!");
            console.log(registeredUsers);
    }
}
function addFriend(eUser) {
    if(friendsList.includes(eUser)){
        alert("User is already added on the friends list");
    }else{
        if(registeredUsers.includes(eUser)){
                friendsList.push(eUser);
                alert("You have added " + eUser + " as a friend!");
                console.log(friendsList);
        }else{
            alert("User not found.")

        }
    }
}
function displayFriends() {
    if(friendsList.length == 0){
        alert("You currently have 0 friends. Add one first.");
    }else{
        for (var i = 0; i <= friendsList.length - 1; i++) {
            console.log(friendsList[i]);
        }
    }

}
function displayNumberOfFriends() {
     if(friendsList.length == 0){
        alert("You currently have 0 friends. Add one first.");
    }else{
        console.log("You currently have " + friendsList.length + " friends.");
    }
}function deleteFriend() {
    if(friendsList.length == 0){
        alert("You currently have 0 friends. Add one first.");
    }else{
        friendsList.pop();
        console.log(friendsList)
    }
    
}
function removeUser(name) {
    if(friendsList.length == 0){
        alert("You currently have 0 friends. Add one first.");
    }else{
        for (var i = 0; i <= friendsList.length - 1; i++) {
            if(friendsList[i] === name){
                friendsList.splice(i,1);
                console.log(friendsList)
            }
        }
    }
}